# kio_fish Bahasa Melayu (Malay) (ms)
# Copyright (C) 2008, 2009 K Desktop Environment
# Muhammad Najmi bin Ahmad Zabidi <md_najmi@yahoo.com>
# Sharuzzaman Ahmat Raslan <sharuzzaman@myrealbox.com>, 2004, 2008, 2009.
#
msgid ""
msgstr ""
"Project-Id-Version: kio_fish\n"
"Report-Msgid-Bugs-To: https://bugs.kde.org\n"
"POT-Creation-Date: 2022-10-21 00:46+0000\n"
"PO-Revision-Date: 2009-05-28 00:55+0800\n"
"Last-Translator: Sharuzzaman Ahmat Raslan <sharuzzaman@myrealbox.com>\n"
"Language-Team: Malay <kedidiemas@yahoogroups.com>\n"
"Language: ms\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"X-Generator: KBabel 1.11.4\n"
"Plural-Forms: nplurals=2; plural=1;\n"

#: fish.cpp:291
#, kde-format
msgid "Connecting..."
msgstr "Menghubungkan..."

#: fish.cpp:593
#, kde-format
msgid "Initiating protocol..."
msgstr "Memulakan protokol..."

#: fish.cpp:629
#, kde-format
msgid "Local Login"
msgstr "Login Tempatan"

#: fish.cpp:631
#, fuzzy, kde-format
#| msgid "SSH Authorization"
msgid "SSH Authentication"
msgstr "Membenarkan SSH"

#: fish.cpp:670 fish.cpp:690
#, kde-format
msgctxt "@action:button"
msgid "Yes"
msgstr ""

#: fish.cpp:670 fish.cpp:690
#, kde-format
msgctxt "@action:button"
msgid "No"
msgstr ""

#: fish.cpp:771
#, kde-format
msgid "Disconnected."
msgstr "Terputus."

#~ msgctxt "NAME OF TRANSLATORS"
#~ msgid "Your names"
#~ msgstr "Sharuzzaman Ahmat Raslan"

#~ msgctxt "EMAIL OF TRANSLATORS"
#~ msgid "Your emails"
#~ msgstr "sharuzzaman@myrealbox.com"
