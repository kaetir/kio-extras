# translation of kio_sftp.po to Tajik
# translation of kio_sftp.po to Тоҷикӣ
# Copyright (C) 2004 Free Software Foundation, Inc.
# 2004, infoDev, a World Bank organization
# 2004, Khujand Computer Technologies, Inc.
# 2004, KCT1, NGO
# Dilshod Marupov <dma165@hotmail.com>, 2004
#
msgid ""
msgstr ""
"Project-Id-Version: kio_sftp\n"
"Report-Msgid-Bugs-To: https://bugs.kde.org\n"
"POT-Creation-Date: 2022-11-18 00:44+0000\n"
"PO-Revision-Date: 2012-01-04 21:54+0500\n"
"Last-Translator: Victor Ibragimov <victor.ibragimov@gmail.com>\n"
"Language-Team: Tajik\n"
"Language: tg\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"X-Generator: KBabel 1.0.1\n"
"Plural-Forms: nplurals=2; plural=n != 1;\n"

#: kio_sftp.cpp:226
#, fuzzy, kde-format
#| msgid "Incorrect username or password"
msgid "Incorrect or invalid passphrase"
msgstr "Номи корванд ё гузарвожаи нодуруст"

#: kio_sftp.cpp:276
#, kde-format
msgid "Could not allocate callbacks"
msgstr "Не удалось выделить память под функции обработки"

#: kio_sftp.cpp:289
#, kde-format
msgid "Could not set log verbosity."
msgstr "Не удалось установить уровень журналирования."

#: kio_sftp.cpp:294
#, fuzzy, kde-format
#| msgid "Could not set log verbosity."
msgid "Could not set log userdata."
msgstr "Не удалось установить уровень журналирования."

#: kio_sftp.cpp:299
#, fuzzy, kde-format
#| msgid "Could not allocate callbacks"
msgid "Could not set log callback."
msgstr "Не удалось выделить память под функции обработки"

#: kio_sftp.cpp:335 kio_sftp.cpp:337 kio_sftp.cpp:854
#, kde-format
msgid "SFTP Login"
msgstr "Номнависӣ дар SFTP"

#: kio_sftp.cpp:352
#, kde-format
msgid "Use the username input field to answer this question."
msgstr "Используйте поле имени пользователя для ответа на этот вопрос."

#: kio_sftp.cpp:365
#, fuzzy, kde-format
#| msgid "Please enter your username and password."
msgid "Please enter your password."
msgstr "Номи корванд ва гузарвожаи худро ворид кунед."

#: kio_sftp.cpp:370 kio_sftp.cpp:857
#, fuzzy, kde-format
#| msgid "site:"
msgid "Site:"
msgstr "сайт:"

#: kio_sftp.cpp:417
#, fuzzy, kde-format
#| msgid "Could not read link: %1"
msgctxt "error message. %1 is a path, %2 is a numeric error code"
msgid "Could not read link: %1 [%2]"
msgstr "Не удалось прочитать ссылку: %1"

#: kio_sftp.cpp:539
#, kde-format
msgid "Could not create a new SSH session."
msgstr "Не удалось открыть сеанс SSH."

#: kio_sftp.cpp:550 kio_sftp.cpp:554
#, kde-format
msgid "Could not set a timeout."
msgstr "Не удалось установить время ожидания."

#: kio_sftp.cpp:561
#, fuzzy, kde-format
#| msgid "Could not set port."
msgid "Could not disable Nagle's Algorithm."
msgstr "Не удалось установить номер порта."

#: kio_sftp.cpp:567 kio_sftp.cpp:572
#, kde-format
msgid "Could not set compression."
msgstr "Не удалось установить сжатие."

#: kio_sftp.cpp:578
#, kde-format
msgid "Could not set host."
msgstr "Не удалось установить имя сервера."

#: kio_sftp.cpp:584
#, kde-format
msgid "Could not set port."
msgstr "Не удалось установить номер порта."

#: kio_sftp.cpp:592
#, kde-format
msgid "Could not set username."
msgstr "Не удалось установить имя пользователя."

#: kio_sftp.cpp:599
#, kde-format
msgid "Could not parse the config file."
msgstr "Не удалось обработать конфигурационный файл."

#: kio_sftp.cpp:616
#, fuzzy, kde-kuit-format
#| msgid "Opening SFTP connection to host %1:%2"
msgid "Opening SFTP connection to host %1:%2"
msgstr "Кушодани пайвастшавии SFTP бо шабакаи %1:%2"

#: kio_sftp.cpp:656
#, fuzzy, kde-format
#| msgid "Could not set username."
msgid "Could not get server public key type name"
msgstr "Не удалось установить имя пользователя."

#: kio_sftp.cpp:669
#, kde-format
msgid "Could not create hash from server public key"
msgstr ""

#: kio_sftp.cpp:678
#, kde-format
msgid "Could not create fingerprint for server public key"
msgstr ""

#: kio_sftp.cpp:738
#, fuzzy, kde-format
#| msgid ""
#| "The host key for this server was not found, but another type of key "
#| "exists.\n"
#| "An attacker might change the default server key to confuse your client "
#| "into thinking the key does not exist.\n"
#| "Please contact your system administrator.\n"
#| "%1"
msgid ""
"An %1 host key for this server was not found, but another type of key "
"exists.\n"
"An attacker might change the default server key to confuse your client into "
"thinking the key does not exist.\n"
"Please contact your system administrator.\n"
"%2"
msgstr ""
"Для этого сервера не найден ключ узла, но присутствует другой тип ключа.\n"
"Атакующий злоумышленник может подменить ключ сервера, что вызывает подобную "
"ситуацию.\n"
"Обратитесь к системному администратору.\n"
"%1"

#: kio_sftp.cpp:756
#, fuzzy, kde-format
#| msgid "Warning: Host's identity changed."
msgctxt "@title:window"
msgid "Host Identity Change"
msgstr "Диққат: Шиносаи соҳиб тағир дода шуд!"

#: kio_sftp.cpp:757
#, fuzzy, kde-kuit-format
#| msgid ""
#| "The host key for the server %1 has changed.\n"
#| "This could either mean that DNS SPOOFING is happening or the IP address "
#| "for the host and its host key have changed at the same time.\n"
#| "The fingerprint for the key sent by the remote host is:\n"
#| " %2\n"
#| "Please contact your system administrator.\n"
#| "%3"
msgctxt "@info"
msgid ""
"<para>The host key for the server <emphasis>%1</emphasis> has changed.</"
"para><para>This could either mean that DNS spoofing is happening or the IP "
"address for the host and its host key have changed at the same time.</"
"para><para>The %2 key fingerprint sent by the remote host is:<bcode>%3</"
"bcode>Are you sure you want to continue connecting?</para>"
msgstr ""
"Изменён ключ сервера %1.\n"
"Это означает, что возможна подмена адреса сервера.\n"
"Отпечаток ключа, полученный сейчас с сервера: %2\n"
"Обратитесь к системному администратору.\n"
"%3"

#: kio_sftp.cpp:767
#, kde-format
msgctxt "@title:window"
msgid "Host Verification Failure"
msgstr ""

#: kio_sftp.cpp:768
#, fuzzy, kde-kuit-format
#| msgid ""
#| "The authenticity of host %1 cannot be established.\n"
#| "The key fingerprint is: %2\n"
#| "Are you sure you want to continue connecting?"
msgctxt "@info"
msgid ""
"<para>The authenticity of host <emphasis>%1</emphasis> cannot be established."
"</para><para>The %2 key fingerprint is:<bcode>%3</bcode>Are you sure you "
"want to continue connecting?</para>"
msgstr ""
"Не удалось определить аутентичность сервера %1.\n"
"Отпечаток ключа: %2\n"
"Продолжить подключение к серверу?"

#: kio_sftp.cpp:777
#, kde-format
msgctxt "@action:button"
msgid "Connect Anyway"
msgstr ""

#: kio_sftp.cpp:800 kio_sftp.cpp:818 kio_sftp.cpp:833 kio_sftp.cpp:846
#: kio_sftp.cpp:898 kio_sftp.cpp:908
#, kde-format
msgid "Authentication failed."
msgstr "Хатогӣ дар аслшиносӣ."

#: kio_sftp.cpp:806
#, kde-format
msgid ""
"Authentication failed. The server didn't send any authentication methods"
msgstr ""

#: kio_sftp.cpp:855
#, kde-format
msgid "Please enter your username and password."
msgstr "Номи корванд ва гузарвожаи худро ворид кунед."

#: kio_sftp.cpp:866
#, kde-format
msgid "Incorrect username or password"
msgstr "Номи корванд ё гузарвожаи нодуруст"

#: kio_sftp.cpp:915
#, kde-format
msgid ""
"Unable to request the SFTP subsystem. Make sure SFTP is enabled on the "
"server."
msgstr ""
"Не удаётся использовать подсистему SFTP. Убедитесь, что сервер поддерживает "
"SFTP."

#: kio_sftp.cpp:920
#, kde-format
msgid "Could not initialize the SFTP session."
msgstr "Не удалось инициализировать сеанс SFTP."

#: kio_sftp.cpp:924
#, kde-format
msgid "Successfully connected to %1"
msgstr "Пайвастшавии бомувафақият бо %1"

#: kio_sftp.cpp:976
#, kde-format
msgid "Invalid sftp context"
msgstr ""

#: kio_sftp.cpp:1546
#, kde-format
msgid ""
"Could not change permissions for\n"
"%1"
msgstr ""
"Невозможно изменить права доступа для\n"
"%1"

#, fuzzy
#~| msgid ""
#~| "The host key for the server %1 has changed.\n"
#~| "This could either mean that DNS SPOOFING is happening or the IP address "
#~| "for the host and its host key have changed at the same time.\n"
#~| "The fingerprint for the key sent by the remote host is:\n"
#~| " %2\n"
#~| "Please contact your system administrator.\n"
#~| "%3"
#~ msgid ""
#~ "The host key for the server %1 has changed.\n"
#~ "This could either mean that DNS SPOOFING is happening or the IP address "
#~ "for the host and its host key have changed at the same time.\n"
#~ "The fingerprint for the %2 key sent by the remote host is:\n"
#~ "  SHA256:%3\n"
#~ "Please contact your system administrator.\n"
#~ "%4"
#~ msgstr ""
#~ "Изменён ключ сервера %1.\n"
#~ "Это означает, что возможна подмена адреса сервера.\n"
#~ "Отпечаток ключа, полученный сейчас с сервера: %2\n"
#~ "Обратитесь к системному администратору.\n"
#~ "%3"

#~ msgid "Warning: Cannot verify host's identity."
#~ msgstr "Диққат: Шиносаи соҳибро муайян карда натавониста истодаам!"

#~ msgid ""
#~ "The host key for this server was not found, but another type of key "
#~ "exists.\n"
#~ "An attacker might change the default server key to confuse your client "
#~ "into thinking the key does not exist.\n"
#~ "Please contact your system administrator.\n"
#~ "%1"
#~ msgstr ""
#~ "Для этого сервера не найден ключ узла, но присутствует другой тип ключа.\n"
#~ "Атакующий злоумышленник может подменить ключ сервера, что вызывает "
#~ "подобную ситуацию.\n"
#~ "Обратитесь к системному администратору.\n"
#~ "%1"

#~ msgid ""
#~ "The authenticity of host %1 cannot be established.\n"
#~ "The key fingerprint is: %2\n"
#~ "Are you sure you want to continue connecting?"
#~ msgstr ""
#~ "Не удалось определить аутентичность сервера %1.\n"
#~ "Отпечаток ключа: %2\n"
#~ "Продолжить подключение к серверу?"

#~ msgid "No hostname specified."
#~ msgstr "Не указано имя сервера."

#~ msgid "An internal error occurred. Please retry the request again."
#~ msgstr ""
#~ "Хатогии дохилӣ рух дод. Марҳамат карда дархостро бори дигар кӯшиш кунед."

#~ msgid "Please enter your username and key passphrase."
#~ msgstr "Номи корванд ва гузарвожаи калидии худро ворид кунед."

#~ msgid "Connection failed."
#~ msgstr "Хатогӣ дар пайвастшавӣ."

#~ msgid "Connection closed by remote host."
#~ msgstr "Пайвастшавӣ аз тарафи соҳиби дурдаст пӯшида шудааст."
